// import { kebabCase } from 'lodash'
import mod1 from 'mod1'
import rxjs from 'rxjs'
console.log('rxjs synchronous import', rxjs)

SystemJS.import('rxjs').then(rxjs => console.log('systemjs import rxjs', rxjs))

mod1.bar()
SystemJS.import(`test`).then(test => console.log('from test', test.thing))
